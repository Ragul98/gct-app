import React, { Component } from 'react';
import {
  AppRegistry, Text, View, AsyncStorage, TextInput, Alert,
  Image,
  Dimensions, StyleSheet,
  Keyboard
} from 'react-native';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkStyleSheet,
  RkTheme,
  RkCard
} from 'react-native-ui-kitten';
import { FontAwesome } from '../../assets/icons';
import '../global';
import { isSignedIn, onSignIn, onSignOut } from "./auth";

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: 'hod@gmail.com',
      password: '123456',
      in: false,
      isloading: false,
      error: '',
      access_token: '',
      refresh_token: '',
      canLogin:false,
      is_approved:false,
      is_verified:false,
    }
  }

  componentDidMount() {
    console.log('Mounted\t\tmounted\n');
    // this.login('xxx', 'xxxx');
    try{
    this.userLogout();
    }catch(error){
      console.log(error);
    }
  }




  async login(username, password) {
    console.log('entering login function \n');
    let oauthLoginEndPointUrl = baseURL + TokenURL;
    var body = `grant_type=password&client_id=${clientId}&client_secret=${clientSecret}&username=${username}&password=${password}`;
    this.state.isloading = true;
    await fetch(oauthLoginEndPointUrl, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      'body': body,

    }).then((response) => response.json())
      .then((responseJson) => {
        this.state.isloading = false;
        console.log(responseJson);
        if (responseJson.error) {
          console.log(responseJson.error);
        }
        res = responseJson;
        msg = '';
        if (res.error) {
          msg = res.error
          if (res.error_description) {
            msg = res.error + res.error_description;
            Alert.alert(msg);
          }
          else {
            Alert.alert(msg);
          }

        } else {

          console.log('successful login');
          token = res.access_token;
          try {
            this.saveItem('access_token', token);
            this.state.access_token = token;
             this.custom_user_detail();
            this.in = true;
            onSignIn();
          } catch (error) {
            this.in = false;
            console.log('AsyncStorage error: ' + error.message);
            msg = 'storage error please check the permission';
            return ;
          }
          this.state.refresh_token = res.refresh_token;
          this.saveItem('refresh_token', res.refresh_token);
          return ;
        }

      }).catch((error) => {
        console.log(error);
        Alert.alert('check internet connectivity ')
      });
  }



  async revokeToken() {
    try {
      const logoutURL = baseURL + RevokeURL;
      var token = this.getAccessToken();
      await fetch(logoutURL, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: token,
          client_id: clientId,
          cliend_secret: clientSecret,
        })
      });
    }
    catch (error) {
      console.log(error);
    }
  }

  async custom_user_detail() {

    try {
      const detailUrl = baseURL + UserDetails;
      var token = this.getAccessToken();
      console.log('access token');
      console.log('bearer ' + this.state.access_token);
      await fetch(detailUrl, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${this.state.access_token}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },

      }).then((response) => response.json())
        .then((responseJson) => {
          console.log('user details');
          console.log(responseJson);
          this.state.is_approved =  responseJson.is_approved;
          this.state.is_verified =  responseJson.is_verified;
          console.log(`approved :${responseJson.is_approved}`);
          console.log(`verified :${responseJson.is_verified}`);

         if((responseJson.is_staff_account===true)&&(responseJson.is_approved===true)&&(responseJson.is_verified===true))
          {
            this.state.canLogin=true;
            console.log('can login');
            Alert.alert('login success');
            this.props.navigation.navigate('SignedIn');
          }else{
            console.log(`can't login`);
            Alert.alert(`not a staff account`)
            this.revokeToken();
            onSignOut();
          }
          if (responseJson.error) {
            console.log(responseJson.error);
          }
        });
    }
    catch (error) {
      console.log(error);
    }

  }

  async exchangetoken() {
    console.log('entering exchange \n');
    let oauthLoginEndPointUrl = baseURL + TokenURL;

    var exToken = await this.getRefreshToken();
    console.log(this.state.refresh_token);
    var body = `grant_type=refresh_token&client_id=${clientId}&client_secret=${clientSecret}&refresh_token=${this.state.refresh_token}`;
    console.log(body);
    this.state.isloading = true;
    await fetch(oauthLoginEndPointUrl, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      'body': body,

    }).then((response) => response.json())
      .then((responseJson) => {
        this.state.isloading = false;
        console.log('refreshed token');
        console.log(responseJson);
        console.log('----------');
        try {
          token = responseJson.access_token;
          this.saveItem('access_token', token);
          this.in = true;
          this.saveItem('refresh_token', responseJson.refresh_token);
          onSignIn();
          this.props.navigation.navigate('SignedIn');
        } catch (error) {
          console.log(error);
        }

      }).catch(error)
    {
      console.log(error);
    };
  }




  async userLogout() {
    try {
      await AsyncStorage.removeItem('access_token');
      this.revokeToken();
      await onSignOut();
      this.props.navigation.navigate('SignedOut');


      Alert.alert('Logout Success!');
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  async getAccessToken() {
    await AsyncStorage.getItem('access_token').then(token => {
      console.log('get access token');
      console.log(token);
      this.state.access_token = token;
      return token;
    });
  }


  async  getRefreshToken() {
    await AsyncStorage.getItem('refresh_token').then(token => {
      this.state.refresh_token = token;
      console.log(token);

      return token;
    });
  }


  onChangeText(value) {
    this.setState({
      username: value
    });
  }


  OnChangepass(value) {
    this.setState({
      password: value
    });
  }


  onSubmit() {
    console.log('submitted ');
  }

  isValid() {
    let valid = false;
    username = this.state.username;
    password = this.state.password;
    if (username.length > 4 && password.length > 4) {
      valid = true;
    }

    if (username.length === 0) {
      this.setState({ error: 'You must enter an email address' });
    } else if (password.length === 0) {
      this.setState({ error: 'You must enter a password' });
    } else if (username.length < 6) {
      this.setState({ error: 'You must enter a valid email address' });
    } else if (password.length < 4) {
      this.setState({ error: 'password length must be greater than 6' });

    }

    return valid;
  }


  //todo next page
  logincheck() {
    if (this.isValid()) {

      this.login(this.state.username, this.state.password);
    } else {
      Alert.alert(this.state.error);
    }

  }


  render() {
    //todo background image add
    console.log('\n\n\n\nin  render\n\n\n\n');
    return (

      <View style={{ backgroundColor: RkTheme.current.colors.success }}>

        <RkCard  >

          <View rkCardHeader  >
            <RkText rkType='header'>GCTPortal</RkText>
          </View>
          <RkAvoidKeyboard>
            <RkTextInput rkType='rounded' value={this.state.username}
              onChangeText={(value) => this.onChangeText(value)}
              onSubmitEditing={this.onSubmit}
              placeholder='email' />
            <RkTextInput rkType='rounded'
              placeholder='Password'
              value={this.state.password}
              onChangeText={(value) => this.OnChangepass(value)}
              onSubmitEditing={this.onSubmit}
              secureTextEntry={true} />
            <RkButton onPress={this.logincheck.bind(this)}>Login</RkButton>
          </RkAvoidKeyboard>
        </RkCard>

      </View>
    );
  }
}

//TODO add styles

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "stretch"
  },
  cover: {
    flex: 1,
    width: null,
    height: null
  }
});


AppRegistry.registerComponent('Login', () => Login);

