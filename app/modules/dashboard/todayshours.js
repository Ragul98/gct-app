import React, { Component } from 'react';
import {
  AppRegistry, Text, View, AsyncStorage, TextInput, Alert,
  Image,
  Button,
  Dimensions, StyleSheet,
  Keyboard,
  ActivityIndicator, ListView, Platform, TouchableOpacity
} from 'react-native';

// import MaterialIcons from 'react-native-vector-icons/Material/Icons';
import { get } from '../../util';
import '../global';

export default class Todayshours extends Component {
  static navigationOptions = {
    tarBarLabel: 'Todays hours',
    // drawerIcon:()=>{

    //   return();
    // }


  }
  GetItem(student_name) {

    Alert.alert(student_name);

  }

  state = {
    isLoading: true,
  }

  constructor() {
    super();
    let table = get(baseURL + TodaysHours);
    table.then(tabarr => {
      let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
      this.setState({
        isLoading: false,
        dataSource: ds.cloneWithRows(tabarr),
      }, function () {


        for(i=1;i<9;i++)
        {
  
            console.log(this.state.dataSource[i]);
        }      });
    });
  }

  ListViewItemSeparator = () => {
    return (
      <View
        style={{

          height: .5,
          width: "100%",
          backgroundColor: "#000",

        }}
      />
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={
          {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }
        }>
          <Text> Loading  </Text>
          {/* <Button
            onPress = {()=>this.props.navigation.navigate('DrawerOpen')}
            title="Open DrawNavigator"
          />  */}

        </View>
      );
    }
    else {

      return (

        <View style={styles.MainContainer}>

          <ListView

            dataSource={this.state.dataSource}

            renderSeparator={this.ListViewItemSeparator}

            renderRow={(rowData) =>

              <View style={{ flex: 1, flexDirection: 'column' }} >

                <TouchableOpacity onPress={this.GetItem.bind(this, rowData.course_id)} >

                  <Text style={styles.textViewContainer} >{'hour = ' + rowData.hour}</Text>
                  <Text style={styles.textViewContainer} >{'Subject = ' + rowData.course_name}</Text>
                </TouchableOpacity>

              </View>

            }
          />

        </View>
      );

    }
  }
}



const styles = StyleSheet.create({

  MainContainer: {

    // Setting up View inside content in Vertically center.
    justifyContent: 'center',
    flex: 1,
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    backgroundColor: '#00BCD4',
    padding: 5,

  },

  textViewContainer: {

    textAlignVertical: 'center',
    padding: 10,
    fontSize: 20,
    color: '#fff',

  }

});


AppRegistry.registerComponent('Todayshours', () => Todayshours);
