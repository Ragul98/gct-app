import {
    StackNavigator,
    TabNavigator,
    SwitchNavigator,
    DrawerNavigator
  } from "react-navigation";
  import Login from './modules/login/login';
  import Todayshours from './modules/dashboard/todayshours'


  export const Dashboard = DrawerNavigator(
    {
      Todayshours : {
        screen:Todayshours
      },
      Logout:{
        screen:Login
      }
    }, 
  );


  export const createRootNavigator = (signedIn = false) => {
    return SwitchNavigator(
      {
        SignedIn: {
          screen: Dashboard
        },
        SignedOut: {
          screen: Login
        }
      },
      {
        initialRouteName: signedIn ? "SignedIn" : "SignedOut"
      }
    );
  };