import {
    AppRegistry, Text, View, AsyncStorage, TextInput, Alert,
    Image,
    Dimensions, StyleSheet,
    Keyboard
  } from 'react-native';

export async function get(URL)
{   
    token = await AsyncStorage.getItem('access_token');
    console.log(token);

    let  response = await fetch(URL, 
    { 
        method: 'GET', 
        headers: {
            'Authorization': 'Bearer '+ token,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          }, 
    }   );
    console.log(response);
    let body = await response.json();
    console.log(body);
    return body;
}