import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';


import { isSignedIn } from "./app/modules/login/auth";
import Login from './app/modules/login/login';
import Todayshours from './app/modules/dashboard/todayshours';
import {createRootNavigator} from './app/router';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  }

  
  componentDidMount() {
    isSignedIn()
      .then(res => {
        this.setState({ signedIn: res, checkedSignIn: true });
        console.log(this.state);
      }
    )
      .catch(err => alert("An error occurred"));
  }


  render() {
    const { checkedSignIn, signedIn } = this.state;

    if (!checkedSignIn) {
      return null;
    }
    
    const Layout = createRootNavigator(signedIn);
    return <Layout />;
  }
}

