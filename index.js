import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('gct_app', () => App);
