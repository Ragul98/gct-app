# GCT APP

## Requirements for first time:
- android studio
- android sdk - 6.0 
- npm and nodejs


```bash
$ cd path/to/gct_app/
$ npm install 
```

## To run the code

Run this to start a js server

```bash
react-native start
```

Open the emulator and start the virtual device

```bash
react-native run-android
```


note: source files in  /modules/
